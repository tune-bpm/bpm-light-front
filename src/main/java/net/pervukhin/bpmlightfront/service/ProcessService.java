package net.pervukhin.bpmlightfront.service;

import net.pervukhin.bpmlightfront.dto.Process;
import net.pervukhin.bpmlightfront.dto.PaginationContainerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProcessService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${backend.endpoints.processList}")
    private String processListUrl;

    @Value("${backend.endpoints.processById}")
    private String processById;

    public PaginationContainerDTO<Process> getList(Integer firstResults, Integer maxResults, Boolean isActive) {
        return restTemplate.exchange(processListUrl
                        .replace("{firstResults}", firstResults.toString())
                        .replace("{maxResults}", maxResults.toString())
                        .replace("{isActive}", isActive.toString()),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<PaginationContainerDTO<Process>>() {}).getBody();
    }

    public Process getOne(String processInstanceId) {
        return restTemplate.exchange(processById
                        .replace("{processInstanceId}", processInstanceId),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Process>() {}).getBody();
    }
}
