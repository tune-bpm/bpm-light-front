package net.pervukhin.bpmlightfront.service;

import net.pervukhin.bpmlightfront.dto.Model;
import net.pervukhin.bpmlightfront.dto.PaginationContainerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ModelService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${backend.endpoints.modelList}")
    private String modelListUrl;

    public PaginationContainerDTO<Model> getList(Integer firstResults, Integer maxResults) {
        return restTemplate.exchange(modelListUrl
                        .replace("{firstResults}", firstResults.toString())
                        .replace("{maxResults}", maxResults.toString()),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<PaginationContainerDTO<Model>>() {}).getBody();
    }
}
