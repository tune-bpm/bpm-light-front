package net.pervukhin.bpmlightfront.service;

import net.pervukhin.bpmlightfront.dto.Deployment;
import net.pervukhin.bpmlightfront.dto.PaginationContainerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DeploymentService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${backend.endpoints.deploymentList}")
    private String deploymentListUrl;

    @Value("${backend.endpoints.deployment}")
    private String deployUrl;

    @Value("${backend.endpoints.deploymentById}")
    private String getByIdUrl;

    public PaginationContainerDTO<Deployment> getList(String deploymentKey, Integer firstResults, Integer maxResults) {
        return restTemplate.exchange(deploymentListUrl
                        .replace("{deploymentKey}", deploymentKey)
                        .replace("{firstResults}", firstResults.toString())
                        .replace("{maxResults}", maxResults.toString()),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<PaginationContainerDTO<Deployment>>() {}).getBody();
    }

    public Deployment getById(String processKey) {
        return restTemplate.exchange(getByIdUrl
                .replace("{processKey}", processKey),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Deployment>() {}).getBody();
    }


    public void deploy(Deployment deployment) {
        restTemplate.postForObject(deployUrl, deployment, Void.class);
    }
}
