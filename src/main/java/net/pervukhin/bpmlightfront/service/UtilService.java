package net.pervukhin.bpmlightfront.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class UtilService {
    @Value("${timezone}")
    private String timezone;

    private String datetimeFormat;

    private DateTimeFormatter dateFormatter;

    public UtilService(@Value("${timezone}") String timezone, @Value("${datetimeFormat}") String datetimeFormat) {
        this.timezone = timezone;
        this.datetimeFormat = datetimeFormat;
        this.dateFormatter = DateTimeFormatter.ofPattern(this.datetimeFormat);
    }

    public long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public String getDateAsString(Date date) {
        if (date != null) {
            return dateFormatter.format(date.toInstant().atZone(ZoneId.of(timezone)));
        } else {
            return "";
        }
    }

    public String getRunTime(Date startDate, Date endDate) {
        if (endDate == null) {
            return "";
        } else {
            return String.valueOf(Double.valueOf(
                    getDateDiff(startDate, endDate, TimeUnit.MILLISECONDS)) / 1000);
        }
    }
}
