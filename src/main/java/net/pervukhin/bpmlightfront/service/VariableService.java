package net.pervukhin.bpmlightfront.service;

import net.pervukhin.bpmlightfront.dto.PaginationContainerDTO;
import net.pervukhin.bpmlightfront.dto.Variable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class VariableService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${backend.endpoints.variableList}")
    private String variableListUrl;

    public PaginationContainerDTO<Variable> getList(Integer firstResults, Integer maxResults, String processInstanceId) {
        return restTemplate.exchange(variableListUrl
                        .replace("{firstResults}", firstResults.toString())
                        .replace("{maxResults}", maxResults.toString())
                        .replace("{processInstanceId}", processInstanceId),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<PaginationContainerDTO<Variable>>() {}).getBody();
    }
}
