package net.pervukhin.bpmlightfront.service;

import net.pervukhin.bpmlightfront.dto.Activity;
import net.pervukhin.bpmlightfront.dto.PaginationContainerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ActivityService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${backend.endpoints.activityList}")
    private String activityListUrl;

    public PaginationContainerDTO<Activity> getList(Integer firstResults, Integer maxResults, String processInstanceId) {
        return restTemplate.exchange(activityListUrl
                        .replace("{firstResults}", firstResults.toString())
                        .replace("{maxResults}", maxResults.toString())
                        .replace("{processInstanceId}", processInstanceId),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<PaginationContainerDTO<Activity>>() {}).getBody();
    }
}
