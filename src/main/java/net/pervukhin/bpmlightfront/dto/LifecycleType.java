package net.pervukhin.bpmlightfront.dto;

public enum LifecycleType {
    STARTED("запущен"),
    ENDED("завершен");

    private String caption;

    LifecycleType(String caption) {
        this.caption = caption;
    }

    public String getCaption() {
        return caption;
    }
}
