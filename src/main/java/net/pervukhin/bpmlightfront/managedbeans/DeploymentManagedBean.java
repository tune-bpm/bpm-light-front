package net.pervukhin.bpmlightfront.managedbeans;

import net.pervukhin.bpmlightfront.dto.Deployment;
import net.pervukhin.bpmlightfront.dto.Model;
import net.pervukhin.bpmlightfront.dto.PaginationContainerDTO;
import net.pervukhin.bpmlightfront.service.DeploymentService;
import net.pervukhin.bpmlightfront.service.ModelService;
import net.pervukhin.bpmlightfront.service.UtilService;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.ManagedBean;
import javax.faces.view.ViewScoped;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Map;

@ViewScoped
@ManagedBean
public class DeploymentManagedBean {
    @Autowired
    private DeploymentService deploymentService;

    @Autowired
    private ModelService modelService;

    @Autowired
    private UtilService utilService;

    private ModelModel models = new ModelModel();

    private DeploymentModel deployments;

    private Model selectedModel;
    private Deployment selectedDeployment;

    public ModelModel getModels() {
        return models;
    }

    public DeploymentModel getDeployments() {
        return deployments;
    }

    public String getDateString(Date date) {
        return utilService.getDateAsString(date);
    }

    public void upload(FileUploadEvent event) throws IOException {
        Deployment deployment = new Deployment();
        deployment.setFileName(event.getFile().getFileName());
        deployment.setXml(getFileContent(event.getFile().getInputStream()));

        deploymentService.deploy(deployment);
    }

    private String getFileContent(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[8192];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1)
        {
            baos.write(buffer, 0, bytesRead);
        }
        byte[] tmp = baos.toByteArray();
        return new String(tmp, StandardCharsets.UTF_8);
    }

    public Deployment getSelectedDeployment() {
        return selectedDeployment;
    }

    public void setSelectedDeployment(Deployment selectedDeployment) {
        this.selectedDeployment = selectedDeployment;
    }

    public Model getSelectedModel() {
        return selectedModel;
    }

    public void setSelectedModel(Model selectedModel) {
        this.selectedModel = selectedModel;
    }

    public void selectModel() {
        this.deployments = new DeploymentModel(selectedModel.getDeploymentKey());
    }

    class DeploymentModel extends LazyDataModel<Deployment> {
        private String deploymentKey;

        public DeploymentModel(String deploymentKey) {
            this.deploymentKey = deploymentKey;
        }

        @Override
        public List<Deployment> load(int first, int pageSize, Map<String, SortMeta> sort, Map<String, FilterMeta> filter) {
            final PaginationContainerDTO<Deployment> result = deploymentService.getList(this.deploymentKey,
                    first, pageSize);
            this.setRowCount(result.getCount());
            return result.getList();
        }
    }

    class ModelModel extends LazyDataModel<Model> {
        @Override
        public String getRowKey(Model object) {
            return object.getDeploymentKey();
        }

        @Override
        public Model getRowData(String rowKey) {
            final Model model = new Model();
            model.setDeploymentKey(rowKey);
            return model;
        }

        @Override
        public List<Model> load(int first, int pageSize, Map<String, SortMeta> sort, Map<String, FilterMeta> filter) {
            final PaginationContainerDTO<Model> result = modelService.getList(first, pageSize);
            this.setRowCount(result.getCount());
            return result.getList();
        }
    }
}
